﻿using System;
using System.IO;
using System.Collections.Generic;

namespace PackageTool
{
	public class FileFilterByExtensionWithout: FileFilterBase
	{
		// 不需要打包的文件后缀
		private List<string> m_lstWithoutExtension;

		public FileFilterByExtensionWithout ()
		{
		}

		public override void Init()
		{
			m_lstWithoutExtension.Clear ();
		}

		// 增加不需要打包的文件后缀名
		public void AddWithoutExtension(string strExtension)
		{
			// 字符串包含点的才算后缀
			if (strExtension.IndexOf ('.') >= 0) 
			{
				m_lstWithoutExtension.Add (strExtension);
			}
		}

		// 是否阻止该文件
		public override bool IsFileShouldBlock(FileInfo file)
		{
			return m_lstWithoutExtension.Contains (file.Extension);
		}
	}
}

