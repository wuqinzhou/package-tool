﻿using System;
using System.IO;
using System.Collections.Generic;

namespace PackageTool
{
	public class FileFilterByExtensionOnly: FileFilterBase
	{
		// 只需要打包的文件后缀
		private List<string> m_lstOnlyExtension;

		public FileFilterByExtensionOnly ()
		{
		}

		public override void Init()
		{
			m_lstOnlyExtension.Clear ();
		}

		// 增加只需要打包的文件后缀名
		public void AddOnlyExtension(string strExtension)
		{
			// 字符串包含点的才算后缀
			if( strExtension.IndexOf ('.') >= 0)
			{
				m_lstOnlyExtension.Add (strExtension);
			}
		}

		// 是否阻止该文件
		public override bool IsFileShouldBlock(FileInfo file)
		{
			// 列表为空不阻止
			if (m_lstOnlyExtension.Count == 0) 
			{
				return false;
			}

			return !m_lstOnlyExtension.Contains (file.Extension);
		}
	}
}

