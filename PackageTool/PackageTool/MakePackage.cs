﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;


namespace PackageTool
{
	public struct tagMyFileInfo
	{
		//文件名（含路径）
		private string 	m_strFileName;

		public string FileName {
			get {
				return m_strFileName;
			}
			set {
				m_strFileName = value;
			}
		}
	
		// 文件大小
		private Int32		m_nLength;

		public Int32 Length {
			get {
				return m_nLength;
			}
			set {
				m_nLength = value;
			}
		}
	}

	public class MakePackage
	{
		// 文件头前面的数据长度 int+byte+int+int
		const Int32 N_BeforeTitleLength = 13;

		// 文件头第行除名字外的长度 short+int+long
		const Int32 N_EachFileTitleLenExcName = 14;

		// 所有的文件名（含路径）
		private List<tagMyFileInfo> m_lstFile;

		// 要打包的目录
		private string m_strDirPath;

		// 文件过滤器
		private List<FileFilterBase> m_lstFilter;

		// 资源类型
		private byte m_byResType;
		public byte ResType {
			get {
				return m_byResType;
			}
			set {
				m_byResType = value;
			}
		}

		// 版本号
		private Int32 m_nVersion;
		public Int32 Version {
			get {
				return m_nVersion;
			}
			set {
				m_nVersion = value;
			}
		}

		// 单例
		private static MakePackage inst;
		public static MakePackage Inst 
		{
			get 
			{
				if (null == inst) 
				{
					inst = new MakePackage ();
				}
				return inst;
			}
		}

		// 构造函数
		private MakePackage ()
		{
			m_lstFile = new List<tagMyFileInfo> ();
			InitData ();
		}

		// 打包所有文件，类入口函数
		public void PackageAllFiles(string dirPath)
		{
			m_lstFile.Clear();
			m_strDirPath = dirPath;

			FindFile (dirPath);

			MakeResFile ();	
		}

		// 初始化数据
		public void InitData()
		{
			m_lstFile.Clear();
			m_lstFilter.Clear ();
			m_strDirPath = "";
			m_byResType = 0;
			m_nVersion = 1;
		}

		// 增加过滤器
		public void AddFilter(FileFilterBase filter)
		{
			m_lstFilter.Add (filter);
		}
			
		//参数dirPath为指定的目录
		private void FindFile(string dirPath) 
		{ 
			//在指定目录及子目录下查找文件
			DirectoryInfo dirBase = new DirectoryInfo(dirPath);

			foreach(DirectoryInfo dirSub in dirBase.GetDirectories())//查找子目录 
			{
				FindFile(dirSub.ToString() + "/");
			}

		
			//查找所有文件
			foreach(FileInfo file in dirBase.GetFiles("*.*") ) 
			{
				bool bAddFile = true;

				// 遍历所有过滤器，有一个过滤器过不去就不添加
				foreach (FileFilterBase filter in m_lstFilter)
				{
					if (filter.IsFileShouldBlock (file))
					{
						bAddFile = false;
						break;
					}
				}

				if (bAddFile)
				{
					//保存文件信息
					AddFileInfo (file);
				}
			}
		}

		// 创建资源包文件
		private void MakeResFile()
		{
			// 打开目录
			DirectoryInfo Dir=new DirectoryInfo(m_strDirPath);

			// 在打包目录的父录目创建同名文件。后缀为.res
			using (FileStream fsResFile = File.Create (Dir.Parent.FullName + "/" + Dir.Name + ".res"))
			{
				if (!fsResFile.CanWrite)
				{
					return;
				}

				// 保存文件头前面部分
				SaveDataBeforeTitle (fsResFile);

				// 保存文件头
				SaveTitle (fsResFile);

				// 保存文件内容
				SaveContent (fsResFile);

				// 关闭文件流
				fsResFile.Close ();
			}
		}

		// 保存文件头前面部分
		private void SaveDataBeforeTitle(FileStream fsResFile)
		{
			// 保存文件大小
			WriteByteData2File( BitConverter.GetBytes(GetResFileTotalLength()), fsResFile);

			// 保存类型
			WriteByteData2File( BitConverter.GetBytes(m_byResType), fsResFile);

			// 保存版本号
			WriteByteData2File( BitConverter.GetBytes(m_nVersion), fsResFile);

			// 保存文件头大小
			WriteByteData2File ( BitConverter.GetBytes(GetResFileTitleLength ()), fsResFile);
		}

		// 保存文件头
		private void SaveTitle(FileStream fsResFile)
		{
			// 文件内容前面的大小
			Int64 nOffset = GetResFileLenBeforContent ();

			foreach (tagMyFileInfo tagInfo in m_lstFile) 
			{
				// 文件名大小
				WriteByteData2File( BitConverter.GetBytes((short)tagInfo.FileName.Length), fsResFile);

				// 文件名
				WriteByteData2File( Encoding.Default.GetBytes (tagInfo.FileName), fsResFile);

				// 文件大小
				WriteByteData2File( BitConverter.GetBytes(tagInfo.Length), fsResFile);

				// 文件内容开始位置
				WriteByteData2File( BitConverter.GetBytes (nOffset), fsResFile);

				// 计算偏移
				nOffset += tagInfo.Length;
			}
		}

		// 保存文件内容
		private void SaveContent(FileStream fsResFile)
		{
			// 查找所有文件
			foreach (tagMyFileInfo tagInfo in m_lstFile)
			{
				// 保存的是相对路径，要加上打包目录的路径
				using (FileStream fsFile = File.OpenRead (m_strDirPath + "/" + tagInfo.FileName)) 
				{
					if (!fsFile.CanRead)
					{
						continue;
					}

					// 读取要打包文件的内容
					byte[] byFile = new byte[tagInfo.Length];
					fsFile.Read (byFile, 0, byFile.Length);

					// 保存内容
					WriteByteData2File (byFile, fsResFile);

					// 关闭文件流
					fsFile.Close ();
				}

			}
		}

		// 获取资源文件的大小
		private Int32 GetResFileTotalLength()
		{
			// 内容前的大小
			Int32 nLength = GetResFileLenBeforContent ();

			// 内容的大小
			foreach (tagMyFileInfo tagInfo in m_lstFile) 
			{
				nLength += tagInfo.Length;
			}

			return nLength;
		}

		// 资源文件在内容前的大小
		private Int32 GetResFileLenBeforContent ()
		{
			return N_BeforeTitleLength + GetResFileTitleLength ();
		}

		// 文件头的大小
		private Int32 GetResFileTitleLength()
		{
			Int32 nLength = 0;

			// 所有文件名的长度和
			foreach(tagMyFileInfo tagFileInfo in m_lstFile)
			{
				nLength += (Int32)tagFileInfo.FileName.Length;
			}

			// 加上每个文件在文件头固定的长度
			nLength += (m_lstFile.Count * N_EachFileTitleLenExcName);

			return nLength;
		}

		// 将字节数据写到文件中
		private void WriteByteData2File(byte[] byData, FileStream fsResFile)
		{
			fsResFile.Write (byData, 0, byData.Length);
		}

		private void AddFileInfo(FileInfo file)
		{
			tagMyFileInfo tagInfo = new tagMyFileInfo();
			//文件名为打包目录的相地路径
			tagInfo.FileName = file.ToString ().Substring(m_strDirPath.Length+1);
			tagInfo.Length = (Int32)file.Length;

			m_lstFile.Add(tagInfo); 
		}
	}
}