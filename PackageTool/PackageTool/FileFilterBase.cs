﻿using System;
using System.IO;

namespace PackageTool
{
	public class FileFilterBase
	{
		public FileFilterBase ()
		{
		}

		public virtual void Init()
		{
		}

		// 是否阻止该文件
		public virtual bool IsFileShouldBlock(FileInfo file)
		{
			return false;
		}
	}
}

