﻿using System;

namespace PackageTool
{
	class MainClass
	{
		// 工具使用为命令行启动。参数为： 程序名 打包目录 只打包的格式后缀 不打包的格式后缀
		public static void Main (string[] args)
		{
			// 输入参数为要打包的文件目录
			if (args.Length > 1) 
			{
				MakePackage.Inst.InitData ();

				// 是否只打包一些特定后缀的文件
				if (args.Length > 2)
				{
					string[] strOnlyExtension = args [2].Split ('|');

					FileFilterByExtensionOnly filter = new FileFilterByExtensionOnly();
					foreach (string strExtension in strOnlyExtension) 
					{
						filter.AddOnlyExtension (strExtension);
					}
					MakePackage.Inst.AddFilter (filter);
				}

				// 是否不打包一些特定后缀的文件
				if (args.Length > 3)
				{
					string[] strWithoutExtension = args [3].Split ('|');

					FileFilterByExtensionWithout filter = new FileFilterByExtensionWithout();
					foreach (string strExtension in strWithoutExtension) 
					{
						filter.AddWithoutExtension (strExtension);
					}
				}

				MakePackage.Inst.PackageAllFiles(args[1]);
			}
		}

		void ParseStringToArrary(string strSrc, out string[] strOut)
		{
			strOut = strSrc.Split ('|');
		}

	}
}
